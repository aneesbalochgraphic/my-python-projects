import tkinter
from PIL import Image, ImageTk
import random
from pathlib import Path
# start project development 

root = tkinter.Tk()
root.geometry("400x400")
root.title("Dice")

# dice image path
dice =['dice_images/dice-1.png', 'dice_images/dice-2.png', 'dice_images/dice-3.png', 'dice_images/dice-4.png', 'dice_images/dice-5.png', 'dice_images/dice-6.png' ]

image1 = ImageTk.PhotoImage(Image.open(random.choice(dice)).resize((200, 230)))
label1 = tkinter.Label(root, image=image1)

# now pack the widget
label1.pack(expand=True)

def rolling_dice():
    image1 = ImageTk.PhotoImage(Image.open(random.choice(dice)).resize((200, 230)))
    label1.configure(image=image1)
    label1.image=image1


button=tkinter.Button(root,text="Roll the dice",fg="green",command=rolling_dice)
#pack a widget in parent widget
button.pack(expand=True)

root.mainloop()

