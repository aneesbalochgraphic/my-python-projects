import tkinter as tk
from tkinter import ttk
from tkinter import *
import png
from pyqrcode import QRCode
import pyqrcode

window = Tk()

window.geometry('300x350')

window.title('python qr coder')


Label(window,text='Let’s Create QR Code',font='arial 15').pack()

# declear a var for entry widget 
var = StringVar()


def create_qrcode():
    s1=var.get()
    qr = pyqrcode.create(s1)
    qr.png('myqr.png', scale = 6)
    Label(window,text='QR Code is created and saved successfully').pack()

# entry for enter url 
e1 = Entry(window,textvariable=var, font='arial 15').pack()

# button to command function 
b1 = Button(window,text='create',bg='pink',command=create_qrcode).pack()

window.mainloop()
