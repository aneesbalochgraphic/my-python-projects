import pyshorteners
import tkinter as tk
from tkinter import ttk
"""
urlshortner in python is small project which short url using pyshorteners
"""

s = pyshorteners.Shortener()

root = tk.Tk()
root.geometry('300x300')
root.title("python url shortner")




# create stringVar
url_var = tk.StringVar()
short_url_var = tk.StringVar()

# create short_url func
def short_url():
    u = url_var.get()
    short_url_var.set("")
    result = s.tinyurl.short(u)
    display_short_url_entry.insert(0,result)
    url_var.set("")

# create entry for get data

name_label = ttk.Label(root, text="Url Shortner")
name_label.pack()
url_entry = ttk.Entry(root, textvariable=url_var)
url_entry.pack(pady=12)


# submit button
submit_btn = ttk.Button(root, text="submit", command=short_url)
submit_btn.pack(pady=6)

# exit btn
exit_btn = ttk.Button(root, text="exit", command=root.destroy)
exit_btn.pack()

#entry to display url after shortning
display_short_url_entry = ttk.Entry(root, textvariable=short_url_var, width=30)
display_short_url_entry.pack(pady=12)

root.mainloop()

