"""
This is a small project of contact book which can save contact in contact.txt file 
"""

def save_contact():
    try:
        # get contact.txt data
        f = open('contact.txt', 'r')
        read_f = print(f.readlines())
        f.close()

        contact_name = str(input("Enter contact name: "))
        contact_no = int(input("Enter phone no:"))
        
        # check if input is empity
        if contact_name == "":
            # print("Enter value")
            raise ValueError('You did not enter any value')
        elif contact_no == "":
            # print("Enter number")
            raise ValueError('You did not enter any value')

        # open txt file
        else:
            f = open('contact.txt', 'a')
            f.write(f'Contact Name : {contact_name} Contact no : {contact_no} \n')
            f.close()
    except ValueError as e:
        print(f'{e}')

save_contact()
