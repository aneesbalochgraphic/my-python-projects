from bs4 import BeautifulSoup
import requests
import pandas as pd

# writs code

url = "https://en.wikipedia.org/wiki/List_of_districts_in_Balochistan"
page = requests.get(url)

soup = BeautifulSoup(page.content, "html.parser")

# find_ul = soup.find_all('span', attrs={'style':'color:green;'})
find_ul = soup.select('ul > li > span', attrs={'style': 'color:blue;'})[84:]
find_ul_data = [data.text.strip() for data in find_ul]
df = pd.DataFrame( find_ul_data)
df.to_csv('/home/aj/balochistan_districy.csv', index=False)

# for index , s in enumerate(find_ul):
#     print(index , s.text.strip())
