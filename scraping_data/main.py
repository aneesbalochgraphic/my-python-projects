from bs4 import BeautifulSoup
import requests
import pandas as pd
# write code here

url = "https://en.wikipedia.org/wiki/List_of_largest_companies_in_the_United_States_by_revenue"

page = requests.get(url)

soup = BeautifulSoup(page.content, 'html.parser')
table = soup.find_all('table')[1]
table_th = table.find_all('th')
world_table_title = [title.text.strip() for title in table_th]
# print(world_table_title)

df = pd.DataFrame(columns = world_table_title)
df

table_tr = table.find_all('tr')
for tr in table_tr[1:]:
    row_data = tr.find_all('td')
    individual_row_data = [data.text.strip() for data in row_data]
    length = len(df)
    df.loc[length] = individual_row_data

df.to_csv('/home/aj/compaines.csv', index=False)
#<table class="wikitable sortable jquery-tablesorter">
